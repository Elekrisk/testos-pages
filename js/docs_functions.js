let sidebar_size = document.getElementsByClassName("sidebar")[0].style.width;
var isHidden = false;

function toggle_sidebar()
{
    if (isHidden)
    {
        show_docs_sidebar();
    }
    else
    {
        hide_docs_sidebar();
    }
}

function show_docs_sidebar()
{
    document.getElementsByClassName("sidebar")[0].style.visibility = 'visible';
    document.getElementById('hide-show-sidebar').style.left = sidebar_size;
    document.getElementById('hide-show-sidebar').children[0].textContent = "<";
    isHidden = false;
}

function hide_docs_sidebar()
{
    document.getElementsByClassName('sidebar')[0].style.visibility = 'hidden';
    document.getElementById('hide-show-sidebar').style.left = '0';
    document.getElementById('hide-show-sidebar').children[0].textContent = ">";
    isHidden = true;
}